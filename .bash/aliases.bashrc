## BEGIN GNU Tools on Mac ##
# These cascade to other Aliases \
# Unless the custom alias uses the same alias #
alias awk='gawk'
alias b2sum='gb2sum'
alias base32='gbase32'
alias base64='gbase64'
alias basename='gbasename'
alias basenc='gbasenc'
alias cat='gcat'
alias chcon='gchcon'
alias chgrp='gchgrp'
alias chmod='gchmod'
alias chown='gchown'
alias chroot='gchroot'
alias cksum='gcksum'
alias comm='gcomm'
alias cp='gcp'
alias csplit='gcsplit'
alias cut='gcut'
alias date='gdate'
alias dbm_dump='gdbm_dump'
alias dbm_load='gdbm_load'
alias dbmtool='gdbmtool'
alias dd='gdd'
alias df='gdf'
alias dir='gdir'
alias dircolors='gdircolors'
alias dirname='gdirname'
alias du='gdu'
alias echo='gecho'
alias egrep='gegrep'
alias env='genv'
alias expand='gexpand'
alias expr='gexpr'
alias factor='gfactor'
alias false='gfalse'
alias fgrep='gfgrep'
alias find='gfind'
alias fmt='gfmt'
alias fold='gfold'
alias grep='ggrep'
alias groups='ggroups'
alias head='ghead'
alias id='gid'
alias indent='gindent'
alias install='ginstall'
alias join='gjoin'
alias kill='gkill'
alias link='glink'
alias ln='gln'
alias locate='glocate'
alias logname='glogname'
alias ls='gls'
alias md5sum='gmd5sum'
alias mkdir='gmkdir'
alias mkfifo='gmkfifo'
alias mknod='gmknod'
alias mktemp='gmktemp'
alias mv='gmv'
alias nice='gnice'
alias nl='gnl'
alias nohup='gnohup'
alias nproc='gnproc'
alias numfmt='gnumfmt'
alias od='god'
alias paste='gpaste'
alias pathchk='gpathchk'
alias pinky='gpinky'
alias pr='gpr'
alias printenv='gprintenv'
alias printf='gprintf'
alias ptx='gptx'
alias pwd='gpwd'
alias readlink='greadlink'
alias realpath='grealpath'
alias rm='grm'
alias rmdir='grmdir'
alias runcon='gruncon'
alias sed='gsed'
alias seq='gseq'
alias sha1sum='gsha1sum'
alias sha224sum='gsha224sum'
alias sha256sum='gsha256sum'
alias sha384sum='gsha384sum'
alias sha512sum='gsha512sum'
alias shred='gshred'
alias shuf='gshuf'
alias sleep='gsleep'
alias sort='gsort'
alias split='gsplit'
alias stat='gstat'
alias stdbuf='gstdbuf'
alias stty='gstty'
alias sum='gsum'
alias sync='gsync'
alias tac='gtac'
alias tail='gtail'
alias tar='gtar'
alias tee='gtee'
alias test='gtest'
alias timeout='gtimeout'
alias touch='gtouch'
alias tr='gtr'
alias true='gtrue'
alias truncate='gtruncate'
alias tsort='gtsort'
alias tty='gtty'
alias uname='guname'
alias unexpand='gunexpand'
alias uniq='guniq'
alias unlink='gunlink'
alias unzip='gunzip'
alias updatedb='gupdatedb'
alias uptime='guptime'
alias users='gusers'
alias vdir='gvdir'
alias wc='gwc'
alias which='gwhich'
alias who='gwho'
alias whoami='gwhoami'
alias xargs='gxargs'
alias yes='gyes'
alias zexe='gzexe'
alias zip='gzip'
## END GNU Tools on Mac ##


## BEGIN Custom GNU Aliases ##

alias ls='gls --color=auto'
alias dir='gdir --color=auto'
alias vdir='gvdir --color=auto'

alias grep='ggrep --color=auto'
alias fgrep='gfgrep --color=auto'
alias egrep='gegrep --color=auto'

alias du='gdu -kh'    # Makes a more readable output.
alias df='gdf -kTh'

alias free='gfree -m -l -t -h'

alias rm='grm -I --preserve-root'
alias cp='gcp -i'
alias mv='gmv -i'

# -> Prevents accidentally clobbering files.
alias mkdir='gmkdir -p'
alias chown='gchown --preserve-root'
alias chmod='gchmod --preserve-root'
alias chgrp='gchgrp --preserve-root'
alias wget='wget -c'

## END Custom GNU Aliases ##

## BEGIN Custom Aliases ##

alias h='history'
alias j='jobs -l'
alias which='type -a'

# Pretty-print of some PATH variables:
alias path='echo -e ${PATH//:/\\n}'
alias libpath='echo -e ${LD_LIBRARY_PATH//:/\\n}'


# some more ls aliases
alias lx='ls -lXB'         #  Sort by extension.
alias lk='ls -lSr'         #  Sort by size, biggest last.
alias lt='ls -ltr'         #  Sort by date, most recent last.
alias lc='ls -ltcr'        #  Sort by/show change time,most recent last.
alias lu='ls -ltur'        #  Sort by/show access time,most recent last.

# The ubiquitous 'll': directories first, with alphanumeric sorting:
alias ll="ls -larthv --group-directories-first"
alias lm='ll |more'        #  Pipe through 'more'
alias lr='ll -R'           #  Recursive ls.
alias la='ll -A'           #  Show hidden files.
alias tree='tree -Csuh'    #  Nice alternative to 'recursive ls' ...

alias cd..="cd .."
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias .2='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'

alias more='less'

alias push='git pull origin master && git push origin master'
alias pull='git pull origin master'
alias clone='git clone $1'

alias myip="curl http://ipecho.net/plain; echo"

alias c=’/usr/bin/clear’
alias cls=’/usr/bin/clear’

alias null=’/dev/null’

alias home='cd ~'

alias root='cd /'

alias python='python3'
alias pip='pip3'

alias top='htop'
alias df='pydf' # Requires > pip3 install pydf

# just a better Docker printout
alias dps='docker ps --format "table {{.Names}}\t{{.RunningFor}}\t{{.Status}}"'

alias lsblk='lsblk -o NAME,SIZE,FSTYPE,TYPE,MOUNTPOINT'