#!/bin/sh
# base16-shell (https://github.com/chriskempson/base16-shell)
# Base16 Shell template by Chris Kempson (http://chriskempson.com)
# Summerfruit Dark scheme by Christopher Corley (http://christop.club/)

themecolor00="15/15/15"    # Base 00 - Black
themecolor01="FF/00/86"    # Base 08 - Red
themecolor02="00/C9/18"    # Base 0B - Green
themecolor03="AB/A8/00"    # Base 0A - Yellow
themecolor04="37/77/E6"    # Base 0D - Blue
themecolor05="AD/00/A1"    # Base 0E - Magenta
themecolor06="1F/AA/AA"    # Base 0C - Cyan
themecolor07="D0/D0/D0"    # Base 05 - White
themecolor08="50/50/50"    # Base 03 - Bright Black
themecolor09=$themecolor01 # Base 08 - Bright Red
themecolor10=$themecolor02 # Base 0B - Bright Green
themecolor11=$themecolor03 # Base 0A - Bright Yellow
themecolor12=$themecolor04 # Base 0D - Bright Blue
themecolor13=$themecolor05 # Base 0E - Bright Magenta
themecolor14=$themecolor06 # Base 0C - Bright Cyan
themecolor15="FF/FF/FF"    # Base 07 - Bright White
themecolor16="FD/89/00"    # Base 09
themecolor17="CC/66/33"    # Base 0F
themecolor18="20/20/20"    # Base 01
themecolor19="30/30/30"    # Base 02
themecolor20="B0/B0/B0"    # Base 04
themecolor21="E0/E0/E0"    # Base 06
themecolor_foreground="D0/D0/D0" # Base 05
themecolor_background="15/15/15" # Base 00

if [ -n "$TMUX" ]; then
  # Tell tmux to pass the escape sequences through
  # (Source: http://permalink.gmane.org/gmane.comp.terminal-emulators.tmux.user/1324)
  put_template() { printf '\033Ptmux;\033\033]4;%d;rgb:%s\033\033\\\033\\' $@; }
  put_template_var() { printf '\033Ptmux;\033\033]%d;rgb:%s\033\033\\\033\\' $@; }
  put_template_custom() { printf '\033Ptmux;\033\033]%s%s\033\033\\\033\\' $@; }
elif [ "${TERM%%[-.]*}" = "screen" ]; then
  # GNU screen (screen, screen-256color, screen-256color-bce)
  put_template() { printf '\033P\033]4;%d;rgb:%s\007\033\\' $@; }
  put_template_var() { printf '\033P\033]%d;rgb:%s\007\033\\' $@; }
  put_template_custom() { printf '\033P\033]%s%s\007\033\\' $@; }
elif [ "${TERM%%-*}" = "linux" ]; then
  put_template() { [ $1 -lt 16 ] && printf "\e]P%x%s" $1 $(echo $2 | sed 's/\///g'); }
  put_template_var() { true; }
  put_template_custom() { true; }
else
  put_template() { printf '\033]4;%d;rgb:%s\033\\' $@; }
  put_template_var() { printf '\033]%d;rgb:%s\033\\' $@; }
  put_template_custom() { printf '\033]%s%s\033\\' $@; }
fi

# 16 color space
put_template 0  $themecolor00
put_template 1  $themecolor01
put_template 2  $themecolor02
put_template 3  $themecolor03
put_template 4  $themecolor04
put_template 5  $themecolor05
put_template 6  $themecolor06
put_template 7  $themecolor07
put_template 8  $themecolor08
put_template 9  $themecolor09
put_template 10 $themecolor10
put_template 11 $themecolor11
put_template 12 $themecolor12
put_template 13 $themecolor13
put_template 14 $themecolor14
put_template 15 $themecolor15

# 256 color space
put_template 16 $themecolor16
put_template 17 $themecolor17
put_template 18 $themecolor18
put_template 19 $themecolor19
put_template 20 $themecolor20
put_template 21 $themecolor21

# foreground / background / cursor color
if [ -n "$ITERM_SESSION_ID" ]; then
  # iTerm2 proprietary escape codes
  put_template_custom Pg D0D0D0 # foreground
  put_template_custom Ph 151515 # background
  put_template_custom Pi D0D0D0 # bold color
  put_template_custom Pj 303030 # selection color
  put_template_custom Pk D0D0D0 # selected text color
  put_template_custom Pl D0D0D0 # cursor
  put_template_custom Pm 151515 # cursor text
else
  put_template_var 10 $themecolor_foreground
  if [ "$BASE16_SHELL_SET_BACKGROUND" != false ]; then
    put_template_var 11 $themecolor_background
    if [ "${TERM%%-*}" = "rxvt" ]; then
      put_template_var 708 $themecolor_background # internal border (rxvt)
    fi
  fi
  put_template_custom 12 ";7" # cursor (reverse video)
fi

# clean up
unset -f put_template
unset -f put_template_var
unset -f put_template_custom