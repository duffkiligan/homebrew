# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# Hide the ZSH warning in Catalina
export BASH_SILENCE_DEPRECATION_WARNING=1

declare -a bashrc_list=(
  '/etc/bashrc'
  "${HOME}/.bash/functions.bashrc"
  "${HOME}/.bash/aliases.bashrc"
  "${HOME}/.bash_aliases"
  "${HOME}/.bash/completion.bashrc"
  "${HOME}/.bash/exports.bashrc"
  "${HOME}/.bash/git.bashrc"
  "${HOME}/.bash/.bashrc"
  "${HOME}/.bash/base16-summerfruit-dark.sh"
  "${HOME}/.bash/colors.bashrc"
)

for i in "${bashrc_list[@]}"; do
  if [ -f ${i} ]; then
    . ${i}
  fi
done

export PS1="\[${c_lblack}\][\t] \[${c_red}\]\u \[${c_cyan}\]{\W}\[${c_lblack}\] -> \[${c_reset}\]"
export PS2="\[${c_lblack}\]-> \[${c_reset}\]"