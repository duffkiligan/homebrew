#!/bin/bash
## curl -s https://bitbucket.org/duffkiligan/homebrew/raw/master/install.sh | bash -
git_directory=/opt/mac_setup

curl -o ${git_directory}/master.zip https://bitbucket.org/duffkiligan/homebrew/get/master.zip
unzip ${git_directory}/master.zip

unzip_dir=${git_directory}/$(ls ${git_directory} | grep homebrew)

cp -r ${unzip_dir}/.bash ~/.bash
cp ${unzip_dir}/.bash_profile ~/.bash_profile
cp ${unzip_dir}/.bashrc ~/.bashrc

# Install homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

/usr/local/bin/brew install --HEAD ${unzip_dir}/ktylerhomebrew.rb

/usr/local/bin/brew gem install solargraph
/usr/local/bin/brew gem install bundler
/usr/local/bin/brew gem install xcpretty
/usr/local/bin/brew gem install chef

# Add to /etc/shells
/usr/bin/chsh -s /usr/local/bin/bash

/usr/local/bin/pip3 install pydf

/usr/local/bin/brew cask install powershell
/usr/local/bin/brew cask install 1password-cli
/usr/local/bin/brew cask install alfred
/usr/local/bin/brew cask install balenaetcher
/usr/local/bin/brew cask install bartender
/usr/local/bin/brew cask install bettertouchtool
/usr/local/bin/brew cask install brackets
/usr/local/bin/brew cask install cyberduck
/usr/local/bin/brew cask install docker
/usr/local/bin/brew cask install docker-toolbox
/usr/local/bin/brew cask install dotnet
/usr/local/bin/brew cask install graphiql
/usr/local/bin/brew cask install gpg-suite
/usr/local/bin/brew cask install hab
/usr/local/bin/brew cask install insomnia
/usr/local/bin/brew cask install mattermost
/usr/local/bin/brew cask install r
/usr/local/bin/brew cask install riot
/usr/local/bin/brew cask install ubersicht
/usr/local/bin/brew cask install vagrant
/usr/local/bin/brew cask install vlc
