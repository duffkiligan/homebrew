require "formula"

# class PowershellRequirement < Requirement
#   fatal true

#   satisfy(:build_env => false) { which("pwsh") }

#   def message; <<~EOS
#     Powershell is required
#       brew cask install powershell
#     EOS
#   end
# end

class Ktylerhomebrew < Formula
  homepage "https://bitbucket.org/duffkiligan/homebrew"
  head homepage + '.git'

  depends_on 'awscli'
  depends_on 'awsume'
  depends_on 'bash'
  depends_on 'bash-completion@2'
  depends_on 'brew-gem'
  depends_on 'cocoapods'
  depends_on 'coreutils'
  depends_on 'diffutils'
  depends_on 'findutils' => 'default-names'
  depends_on 'gawk'
  depends_on 'git'
  depends_on 'gnu-indent' => 'default-names'
  depends_on 'gnu-sed' => 'default-names'
  depends_on 'gnu-tar' => 'default-names'
  depends_on 'gnu-which' => 'default-names'
  depends_on 'gpatch'
  depends_on 'grep' => 'default-names'
  depends_on 'gzip'
  depends_on 'less'
  depends_on 'openssh'
  depends_on 'python3'
  depends_on 'rsync'
  depends_on 'ruby'
  depends_on 'sshuttle'
  depends_on 'vim' => 'override-system-vi'
  depends_on 'watch'
  depends_on 'wdiff' => 'with-gettext'
  depends_on 'wget'
  depends_on 'zsh'

  def install
    opoo 'All done'
  end

end